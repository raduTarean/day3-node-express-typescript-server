import express, { Request, Response } from "express";
import { VoidExpression } from "typescript";

const app = express();

app.use(express.json());

app.get("/", (req, res) => {
  res.send("Express + TypeScript Server");
});

const server = app.listen(3000, () => {
  console.log("Server started on port 3000");
});
